NODE=$(PWD)/node_modules/.bin

build:
	$(NODE)/tsc


install: node_modules typings


node_modules:
	npm install

typings:
	typings install


run:
	react-native run-android


release-android:
	cd android/
	./gradlew assembleRelease
	cd ../


release-android-install:
	adb uninstall com.zxcvbn
	cd android
	./gradlew installRelease
	cd ../


.PHONY: build install run release-android release-android-install
