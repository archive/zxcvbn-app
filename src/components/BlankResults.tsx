import React, { Component }from 'react';

import {
  Text,
  View,
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    } as React.ViewStyle,
});

interface Props {
}


export default function BlankResults(props : Props) {
  return (
    <View style={styles.container}>
      <Text>Something</Text>
    </View>
  );
}
