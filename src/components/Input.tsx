import React, { Component }from 'react';

import { TextInput, View, StyleSheet } from 'react-native';
import { BRAND_PRIMARY } from '../settings/styles';

const styles = StyleSheet.create({
    inputBorder: {
      borderWidth: 1,
      borderColor: BRAND_PRIMARY,
      borderStyle: 'solid',
      borderRadius: 7,
      margin: 20
    } as React.ViewStyle,

    input: {
      height: 40,
      fontSize: 14,
      color: '#000',
      textAlign: 'center',
    } as React.TextStyle
});

interface State {
  value: string
}

interface Props {
  value: string;
  onSubmit: (text: string) => void;
}

export default class Input extends Component<Props, State> {
  constructor(props : Props) {
    super();
    this.state = {
      value: props.value
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(value : string) {
    this.setState({ value });
  }

  handleSubmit() {
    if (this.props.onSubmit) {
      this.props.onSubmit(this.state.value);
    }
  }

  render() {
    return (
      <View style={styles.inputBorder}>
        <TextInput
          style={styles.input}
          value={this.state.value}
          onSubmitEditing={this.handleSubmit}
          onChangeText={this.handleChange}
          autoCapitalize="none"
          underlineColorAndroid="transparent"
          returnKeyType="done"
          autoCorrect={false}
          blurOnSubmit
          enablesReturnKeyAutomatically />
      </View>
    );
  }
}
