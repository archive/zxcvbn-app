import React from 'react';
import zxcvbn from 'zxcvbn';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';
import _ from 'underscore';

const RESULTS_DATA = require('../data/results');

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  } as React.ViewStyle,
  mainText: {
    fontSize: 16,
    marginBottom: 3,
  } as React.TextStyle,
  description: {
    fontSize: 13,
    color: '#888'
  } as React.TextStyle,
  column: {
  } as React.ViewStyle,
  rightColumn: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'flex-end'
  } as React.ViewStyle
});

interface Props {
  data: any;
}



export default function Result(props: Props) {
  return (
    <View style={styles.container}>
      <View style={[styles.column]}>
        <Text style={styles.mainText}>{props.data.key}: {}</Text>
        <Text style={styles.description}>Something else</Text>
      </View>
      <View style={[styles.column, styles.rightColumn]}>
        <Text>foobar</Text>
      </View>
    </View>
  );
}
