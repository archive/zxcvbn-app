// Define extra types to keep typescript happy!

declare module "react-native-vector-icons/FontAwesome" {
  var noTypeInfoYet: any; // any var name here really
  export = noTypeInfoYet;
}

declare module "zxcvbn" {
  var noTypeInfoYet: any; // any var name here really
  export = noTypeInfoYet;
}
