import React, { Component } from 'react';
import {
  Text,
  View,
  Navigator,
  StyleSheet
} from 'react-native';
import './types';

import Routes from './navigation/Routes';
import RouteMapper from './navigation/RouteMapper';
import { NAVBAR_HEIGHT } from './settings/styles';


const styles = StyleSheet.create({
  container: {
    flex: 1,
  } as React.ViewStyle,
  navbar: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#FFF'
  } as React.ViewStyle,
  wrapper: {
    flex: 1,
    marginTop: NAVBAR_HEIGHT
  } as React.ViewStyle
});

export default class App extends Component<any, any> {
  renderScene(route: any, nav : any) {
    const Component = route.component;
    const props = route.props || {};
    return (
      <View style={styles.wrapper}>
        <Component
          nav={nav}
          currentRoute={route}
          {...props} />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Navigator
          renderScene={this.renderScene}
          initialRoute={Routes.main}
          navigationBar={
            <Navigator.NavigationBar
              style={styles.navbar}
              routeMapper={RouteMapper} />
          } />
      </View>
    );
  }
}
