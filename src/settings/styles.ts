import { Platform } from 'react-native';

export const NAVBAR_HEIGHT = (Platform.OS === 'ios' ? 64 : 56);
export const BRAND_PRIMARY = '#1081DE';
export const BRAND_PRIMARY_LIGHT = '#70B1E8';
