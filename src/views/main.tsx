import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';
import Input from '../components/Input';
import Results from '../components/Results';
import BlankResults from '../components/BlankResults';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#FFF',
  } as React.ViewStyle,
});


interface State {
  text: string
}

export default class App extends Component<any, State> {
  constructor() {
    super()
    this.state = {
      text: ''
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(text : string) {
    this.setState({ text });
  }

  render() {
    const results = this.state.text ? (
      <Results value={this.state.text} />
    ) : (
      <BlankResults />
    );
    return (
      <View style={styles.container}>
        <Input onSubmit={this.handleSubmit} value={this.state.text} />
        { results }
      </View>
    );
  }
};
