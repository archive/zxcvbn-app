import React, { Component } from 'react';

import  {
  StyleSheet,
  Text,
  View,
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  } as React.ViewStyle,
  title: {
    fontSize: 20,
    textAlign: 'center',
    color: '#FFF'
  } as React.TextStyle
});

export default class Title extends Component<any, any> {
  render() {
    const title = this.props.route.title || 'zxcvbn';
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{ title }</Text>
      </View>
    );
  }
}
