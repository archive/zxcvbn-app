import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';


import  {
  StyleSheet,
  TouchableHighlight,
  View,
  Text
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  } as React.ViewStyle,
  toolbarButton: {
    paddingHorizontal: 15,
  } as React.ViewStyle,
  text: {
    color: '#000',
    paddingHorizontal: 10
  } as React.TextStyle
});


export default class BackButton extends Component<any, any> {
  goBack() {
    this.props.nav.pop();
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight
          style={styles.toolbarButton}
          underlayColor="transparent"
          onPress={this.goBack.bind(this)}>
          <Text style={styles.text}><Icon name="chevron-left" size={20} /></Text>
        </TouchableHighlight>
      </View>
    );
  }
}
