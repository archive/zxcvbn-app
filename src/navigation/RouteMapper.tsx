import React from 'react';

import {
  Text,
  View
} from 'react-native';

import BackButton from './BackButton';
import InfoButton from './InfoButton';
import Title from './Title'

export default {
  LeftButton: function (route: any, navigator: any, index: number, navState: any) {
    if (index >= 1) {
      return (
        <BackButton nav={navigator} />
      );
    }
    return <View />;
  },

  RightButton: function (route: any, navigator: any, index: number, navState: any) {
    return <InfoButton nav={navigator} />;
  },

  Title: function (route: any, navigator: any, index: number, navState: any) {
    return <Title route={route} />;
  }
}
