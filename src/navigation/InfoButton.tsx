import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';


import {
  StyleSheet,
  TouchableHighlight,
  View,
  Text
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  } as React.ViewStyle,
  toolbarButton: {
    paddingHorizontal: 15,
  } as React.ViewStyle,
  text: {
    color: '#FFF',
    fontSize: 18,
  } as React.TextStyle
});

export default class InfoButton extends Component<any, any> {
  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight
          style={styles.toolbarButton}
          underlayColor="transparent"
          onPress={() => null}>
          <Icon name="info" style={styles.text} />
        </TouchableHighlight>
      </View>
    );
  }
}
