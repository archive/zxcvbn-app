import React from 'react';
import { AppRegistry } from 'react-native';

import App from './dist/index';

AppRegistry.registerComponent('zxcvbn', () => App);
